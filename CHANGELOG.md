# Changelog

## [Unreleased]
### Changed
- Slight modification to this changelog

## [2.2.4.1] - 2019-08-02
### Added
- `pdo_mysql` extension
- This changelog

### Changed
- Versioning has additional index for image changes to the image that
do not affect Phan itself

## [2.2.4.0]
- Initial release

[Unreleased]: https://gitlab.com/andrew.womeldorf/docker-phan/compare/2.2.4.1...master
[2.2.4.1]: https://gitlab.com/andrew.womeldorf/docker-phan/compare/2.2.4.0...2.2.4.1
[2.2.4.0]: https://gitlab.com/andrew.womeldorf/docker-phan/-/tags/2.2.4.0
