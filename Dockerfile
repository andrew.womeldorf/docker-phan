FROM php:7.3-cli-alpine

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN apk upgrade --update && apk add autoconf gcc g++ make

RUN pecl install ast-1.0.1 && docker-php-ext-enable ast
RUN docker-php-ext-install pdo_mysql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer global require "phan/phan:2.2.4" && ln -s /root/.composer/vendor/bin/phan /usr/local/bin/phan

WORKDIR /code
