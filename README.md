# Docker Phan

Run [Phan](https://github.com/phan/phan) from a Docker container.

https://cloud.docker.com/u/andrewwomeldorf/repository/docker/andrewwomeldorf/phan

## Running Phan against a codebase

`docker run -it --rm -v $(pwd):/code andrewwomeldorf/phan:latest phan`

## Tags

As of 2.2.4, tags should be consistant with Phan versions.

First three indicies correspond with the Phan version, final index is as I make updates to that version's image.

E.g.: `2.2.4.0` is my first image of Phan v2.2.4. `2.2.4.1` is the same Phan version, but an update to the image.

- 2.2.4.1 (latest)
- 2.2.4.0
- 1.0.0

## Breaking Changes

How the script was run changed after version 1.0.0. On version 1.0.0, this is how to run the script:

`docker run -it --rm -v $(pwd):/code andrewwomeldorf/phan:1.0.0`

The newer images are smaller, and I think conform to a better practice regarding images.

| Tag | Size |
| --- | --- |
| 1.0.0 | 727MB |
| 2.2.4 | 319MB |

Also, apologies, I don't have the original Dockerfile for 1.0.0...
